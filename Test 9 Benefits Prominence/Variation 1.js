jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' type='text/css' media='screen'>");

function jQueryDefer(method) {
    if (window.jQuery)
        method();
    else
        setTimeout(function() { jQueryDefer(method) }, 50);
}
function start () {
jQuery(".desktop-nav.desktop").after("<div class='opt-pp-cont container'><div id='nav'></div></div>");
jQuery(".header .quick-access div.links").hide();

var proof_points = [
	["Best Price Guarantee",'<i class="fa fa-usd" aria-hidden="true"></i>', "https://www.matshop.com.au/best-price-guarantee"],
	["365 Day Money Back Guarantee",'<i class="fa fa-star" aria-hidden="true"></i>', "https://www.matshop.com.au/90-day-money-back-guarantee"],
	["Same Day Turn-Around*",'<i class="fa fa-truck" aria-hidden="true"></i>', "https://www.matshop.com.au/same-day-turn-around"],
	["Australia's Biggest Range of Mats", '<img src="//cdn.optimizely.com/img/8505590826/22ccfa8d17d34d6aa3e1bf19b2f9e469.png" />', ""],
]

jQuery.each(proof_points, function(){
	jQuery(".opt-pp-cont #nav").append("<div>"+this[1]+"<a href='"+this[2]+"'><div class='optRight'><p>"+this[0]+"</p></a></div></div>");
});
}

jQueryDefer(start)