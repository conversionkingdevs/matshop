function run(x) {
    try {
      jQuery('body').addClass('3-02-03');
       setTimeout(function () {
		jQuery("head").append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/css/swiper.min.css">');
        getScript("https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/js/swiper.min.js",start);
       },500);
    }
    catch (err) {
        setTimeout(function () {
            run(x);
        },x)
    }
   }
run(50);

var doormat = 
        [
	{
		"url": "https://www.matshop.com.au/door-mats/indoor-mats",
		"name": "Indoor Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Indoor-Entrance-Mat_2.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/outdoor-mats",
		"name": "Outdoor Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Outdoor-Entrance-Mats_1.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/recessed-door-mats",
		"name": "Recessed Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Recessed-Entry-Mats.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/commercial-door-mats",
		"name": "Heavy High Traffic Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Heavy-Traffic-Mats.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/coir-door-mats",
		"name": "Coir Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Coir-Mats_1.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/eco-mats",
		"name": "Eco Friendly Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Eco-Friendly-Mats.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/super-absorbent-doormat",
		"name": "Absorbent Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Absorbent-Entrance-Mats..jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/scraper-door-mat",
		"name": "Scraper Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Scraper-Entrance-Mats.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/rubber-mat",
		"name": "Rubber Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Rubber-Mats.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/washable-door-mats",
		"name": "Easy to Clean Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Easy-To-Clean-Mats_3.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/printed-floor-mats",
		"name": "Printed Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Printed-Entrance-Mat-Image_1.jpg"
	},
	{
		"url": "https://www.matshop.com.au/door-mats/custom-door-mat",
		"name": "Custom Size Door Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/Custom-Size-Entrance-Mats.jpg"
	}
],
chairmats = [
	{
		"url": "https://www.matshop.com.au/chair-mats/plastic-floor-mats",
		"name": "Plastic Chair mats",
		"img": "https://www.matshop.com.au/media/catalog/category/plastic-chair-mat-image.jpg"
	},
	{
		"url": "https://www.matshop.com.au/chair-mats/carpet-protector-chair-mats",
		"name": "Carpet Chair Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/chair-mat-for-carpet-with-warranty.png"
	},
	{
		"url": "https://www.matshop.com.au/chair-mats/hard-floor-chair-mats",
		"name": "Hard Floor Chair Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/hard-floor-chair-mat.png"
	},
	{
		"url": "https://www.matshop.com.au/chair-mats/custom-size-chair-mats",
		"name": "Custom Size Chair Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/heavy-duty-custom-made-chair-mat.png"
	}
],
antifatigue =[
	{
		"url": "https://www.matshop.com.au/large-rubber-comfort-safety-mat-with-holes-90-x-150cm",
		"name": "Large Rubber Mat with holes 90 x 150cm",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/m/u/mult-purpose-anti-fatigue-rubber-mat-roll-with_drainage-holes.jpg",
		"price": "$38.00"
	},
	{
		"url": "https://www.matshop.com.au/multi-purpose-anti-fatigue-rubber-mat-roll-with-drainage-holes",
		"name": "Multi Purpose Anti Fatigue Rubber Mat Roll With Drainage Holes",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/m/u/multi-purpose-anti-fatigue-rubber-mat-roll-with-drainage-holes.png",
		"price": "$97.00"
	},
	{
		"url": "https://www.matshop.com.au/non-slip-anti-fatigue-safety-mat-runner",
		"name": "Non slip Anti Fatigue Safety Mat Runner 900mm wide",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/n/o/non-slip-anti-fatigue-safety-mat-runner-900mm-wide.png",
		"price": "$64.90"
	},
	{
		"url": "https://www.matshop.com.au/ultra-comfort-nitrile-rubber-foam-non-slip-safety-mat",
		"name": "Ultra Comfort Nitrile Rubber Foam Non Slip Safety Mat ",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/a/n/anti-slip-nitrile-rubber-grease-resistant-comfort-mat-with-holes_1.jpg",
		"price": "$171.60"
	},
	{
		"url": "https://www.matshop.com.au/anti-slip-nitrile-rubber-greases-resistant-comfort-mat-with-holes",
		"name": "Anti Slip Nitrile Rubber Grease Resistant Comfort Mat with Holes",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/a/n/anti-slip-nitrile-rubber-grease-resistant-comfort-mat-with-holes_1.jpg",
		"price": "$92.75"
	},
	{
		"url": "https://www.matshop.com.au/grid-surface-moulded-rubber-anti-fatigue-comfort-mat",
		"name": "Grid Surface Moulded Rubber Anti Fatigue Comfort Mat Black 600 x 900mm",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/g/r/grid-surface-moulded-rubber-anti-fatigue-comfort-mat-600-x-900mm-black-border.png",
		"price": "$43.00"
	},
	{
		"url": "https://www.matshop.com.au/grid-surface-moulded-rubber-anti-fatigue-comfort-mat-600-x-900-yellow-border",
		"name": "Grid Surface Moulded Rubber Anti Fatigue Comfort Mat 600 x 900 Yellow Border",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/g/r/grid-surface-moulded-rubber-anti-fatigue-comfort-mat-600-x-900mm-yellow-border.png",
		"price": "$55.00"
	}
],
antislip = [
	{
		"url": "https://www.matshop.com.au/high-comfort-durable-heavy-traffic-anti-fatigue-mat",
		"name": "High Comfort Durable Heavy Traffic Anti Fatigue Mat ",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/h/i/high-comfort-durable-heavy-duty-anti-fatigue-mat-yellow-border.jpg",
		"price": "$145.00"
	},
	{
		"url": "https://www.matshop.com.au/pvc-safety-mat-runner",
		"name": "PVC Safety Mat Runner",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/z/e/ze.png",
		"price": "$132.50"
	},
	{
		"url": "https://www.matshop.com.au/anti-slip-nitrile-rubber-greases-resistant-comfort-mat-with-holes",
		"name": "Anti Slip Nitrile Rubber Grease Resistant Comfort Mat with Holes",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/a/n/anti-slip-nitrile-rubber-grease-resistant-comfort-mat-with-holes_1.jpg",
		"price": "$92.75"
	},
	{
		"url": "https://www.matshop.com.au/non-slip-anti-fatigue-safety-mat-runner",
		"name": "Non slip Anti Fatigue Safety Mat Runner 900mm wide",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/n/o/non-slip-anti-fatigue-safety-mat-runner-900mm-wide.png",
		"price": "$64.90"
	},
	{
		"url": "https://www.matshop.com.au/high-traction-low-profile-anti-slip-mat",
		"name": "High Traction Low Profile Anti Slip Mat",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/h/i/high-traction-low-profile-anti-slip-mat.png",
		"price": "$244.60"
	},
	{
		"url": "https://www.matshop.com.au/grease-resistant-rubber-safety-mat-with-holes",
		"name": "Grease Resistant Rubber Safety Mat with Holes",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/g/r/grease-resistant-rubber-safety-mat-with-holes.png",
		"price": "$41.80"
	},
	{
		"url": "https://www.matshop.com.au/durable-rubber-ute-mat-liner-with-holes-1830mm-wide",
		"name": "Heavy Duty Rubber Ute Mat Liner With Holes",
		"img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/u/t/ute-mat-liner-with-holes-1830mm-wide.png",
		"price": "$89.00"
	}
], 
personalisedmat = 
        [
	{
		"url": "https://www.matshop.com.au/personalised-door-mats/indoor-logo-mats",
		"name": "Indoor Logo Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/indoor-logo-mat.png"
	},
	{
		"url": "https://www.matshop.com.au/personalised-door-mats/personalised-outdoor-doormats",
		"name": "Outdoor Logo Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/outdoor-logo-mat-image.png"
	},
	{
		"url": "https://www.matshop.com.au/personalised-door-mats/full-colour-logo-mats",
		"name": "Full Colour Logo Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/full-colour-printed-logo-mat.png"
	},
	{
		"url": "https://www.matshop.com.au/personalised-door-mats/anti-fatigue-logo-mats",
		"name": "Anti Fatigue Logo Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/anti-fatigue-logo-mat-image.png"
	},
	{
		"url": "https://www.matshop.com.au/personalised-door-mats/tiles-logo-mats",
		"name": "Tiles Logo Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/logo-mat-floor-tile-image.png"
	},
	{
		"url": "https://www.matshop.com.au/personalised-door-mats/personalised-bar-mats-runners",
		"name": "Bar- Runner Logo Mats",
		"img": "https://www.matshop.com.au/media/catalog/category/printed-bar-runners.png"
	}
],
matrunners = 
[{
	"items":[
        {
		"url": "https://www.matshop.com.au/mat-runners/carpet-protector-mat",
		"name": "Carpet Protection Mat Runners"
	},
	{
		"url": "https://www.matshop.com.au/mat-runners/non-slip-runner-mats",
		"name": "Non- Slip Mat Runners"
	},
	{
		"url": "https://www.matshop.com.au/mat-runners/outdoor-mat-runner",
		"name": "Outdoor Mat Runners"
	},
	{
		"url": "https://www.matshop.com.au/mat-runners/runner-mats-indoor",
		"name": "Indoor Mat Runners"
	},
	{
		"url": "https://www.matshop.com.au/mat-runners/rubber-floor-tiles",
		"name": "Rubber Tiles"
	},
	{
		"url": "https://www.matshop.com.au/mat-runners/ground-protection-mat-runners",
		"name": "Ground Protection Mat Runners"
	},
	{
		"url": "https://www.matshop.com.au/mat-runners/rubber-mat-runners",
		"name": "Rubber Mat runners"
	}
]},{
    "leftcont": [
        {
            "img":"https://www.matshop.com.au/media/catalog/category/Mat-Runners_1.jpg",
            "url":"https://www.matshop.com.au/mat-runners"
        }
    ]}
],
specialtymats = 
[{
	"items":[
	{
		"url": "https://www.matshop.com.au/specialty-mats/electrical-safety-matting",
		"name": "Electrical Safety Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/anti-static-mats",
		"name": "Anti- Static Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/heat-proof-mats",
		"name": "Fire Resistant Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/welding-mats",
		"name": "Welding Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/kitchen-mats",
		"name": "Kitchen Mats "
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/clean-room-mats",
		"name": "Clean Room Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/non-skid-backing",
		"name": "Non Skid Backing"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/bog-mats",
		"name": "Bog Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/gym-flooring",
		"name": "Gym Flooring"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/ute-mats",
		"name": "Ute Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/pool-shower-mats",
		"name": "Pool & Shower Mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/rubber-ramp-for-wheelchairs",
		"name": "Wheelchair & Mobility Ramp mats"
	},
	{
		"url": "https://www.matshop.com.au/specialty-mats/artificial-grass",
		"name": "Artificial Grass"
	}
]},{
    "leftcont": [
        {
            "img":"https://www.matshop.com.au/media/catalog/category/Specialty-Mats_1.jpg",
            "url":"https://www.matshop.com.au/specialty-mats"
        }
    ]}
],
box = {
    "title":"Create your own personalised mat!",
    "copy":"With a large variety of Logo Mat options we are able to put your logo, image or custom design on a variety of mats.",
    "button":'<button type="button" title="Shop Now" class="button btn-cart shadow-but optShop"><span><span>Shop Now</span></span></button>',
    "image":"//placehold.it/508x151"
};

var boxhtml = '<div class="optPersonalisedMess">    <div class="optContentCont" id="optLeft">            <div class="optHeaderTitle">                <h2>'+box.title+'</h2>            </div>            <div class="optMessP">                <p>'+box.copy+'</p>            </div>            '+box.button+'        </div>        <div class="optContentCont" id="optRight">            <img src="'+box.image+'"/>        </div>    </div>';

// Run the functions to build the test
function start() {
    jQuery('body').addClass('opt1');
    jQuery('.nav-container').prepend('<div class="optMobileCont"></div>');

    // Build best sellers
    jQuery('.products-box').before('<div class="optbestsellerscont swipercont"></div>');
    jQuery('.optbestsellerscont').append(htmlSlidesBuilder(htmlSlidesArr('.products-box li'),"products-grid", "bestsellers"));
    startSwiper('.bestsellers', 5);
    jQuery('.bestsellers.swiper-container').prepend('<h2>Best Sellers</h2>');

    jQuery('.bestsellers.swiper-container').clone().prependTo('.optMobileCont');

    startMobilebestsellers('.optMobileCont .bestsellers.swiper-container', 2, 10);

    jQuery('.optbestsellerscont.swipercont').after(boxhtml);
    jQuery('.optMobileCont .bestsellers.swiper-container').after(boxhtml);
    jQuery('.optShop').click(function (e) {
        e.preventDefault();
        document.location = "https://www.matshop.com.au/personalised-door-mats";
    })


    // Door Mats
    jQuery('.products-box').before('<div class="doormatscont swipercont optgrey"></div>')
    jQuery('.doormatscont').append(buildGreySlide(doormat, undefined, "doormats"));
    startSwiper('.doormats', 4, 10);
    jQuery('.doormatscont').prepend('<h2>Door Mats</h2>');
        //mobile
        jQuery('.optMobileCont').append('<div class="doormatscontM swipercont optgrey"></div>');
        jQuery('.doormatscontM').append(buildGreySlide(doormat, undefined, "doormatsM"));
        startSwiperMobile('.doormatsM', 2, 10);
        jQuery('.doormatscontM').prepend('<h2>Door Mats</h2>');

    // charmats
    jQuery('.products-box').before('<div class="chairmatscont swipercont optgrey"></div>')
    jQuery('.chairmatscont').append(buildGreySlide(chairmats, undefined, "chairmats"));
    startSwiper('.chairmats', 4, 10);
    jQuery('.chairmatscont').prepend('<h2>Chair Mats</h2>');
        //mobile
        jQuery('.optMobileCont').append('<div class="chairmatscontM swipercont optgrey"></div>');
        jQuery('.chairmatscontM').append(buildGreySlide(chairmats, undefined, "chairmatsM"));
        startSwiperMobile('.chairmatsM', 2, 10);
        jQuery('.chairmatscontM').prepend('<h2>Chair Mats</h2>');
    
    // antifatigue
    jQuery('.products-box').before('<div class="antifatiguecont swipercont optgrey productslider"></div>')
    jQuery('.antifatiguecont').append(buildproductslide(antifatigue, undefined, "antifatigue"));
    startSwiper('.antifatigue', 5, 10);
    jQuery('.antifatiguecont').prepend('<h2>Anti Fatigue Mats</h2>');
        //mobile
        jQuery('.optMobileCont').append('<div class="antifatiguecontM swipercont optgrey"></div>');
        jQuery('.antifatiguecontM').append(buildGreySlide(antifatigue, undefined, "antifatigueM"));
        startSwiperMobile('.antifatigueM', 2, 10);
        jQuery('.antifatiguecontM').prepend('<h2>Anti Fatigue Mats</h2>');

    // antislip
    jQuery('.products-box').before('<div class="antislipcont swipercont optgrey productslider"></div>')
    jQuery('.antislipcont').append(buildproductslide(antislip, undefined, "antislip"));
    startSwiper('.antislip', 5, 10);
    jQuery('.antislipcont').prepend('<h2>Anti Slip Mats</h2>');
        //mobile
        jQuery('.optMobileCont').append('<div class="antislipcontM swipercont optgrey"></div>');
        jQuery('.antislipcontM').append(buildGreySlide(antislip, undefined, "antislipM"));
        startSwiperMobile('.antislipM', 2, 10);
        jQuery('.antislipcontM').prepend('<h2>Anti Slip Mats</h2>');

     // charmats
    jQuery('.products-box').before('<div class="personalisedmatcont swipercont optgrey"></div>')
    jQuery('.personalisedmatcont').append(buildGreySlide(personalisedmat, undefined, "personalisedmat"));
    startSwiper('.personalisedmat', 4, 10);
    jQuery('.personalisedmatcont').prepend('<h2>Personalised Mat</h2>');
        //mobile
        jQuery('.optMobileCont').append('<div class="personalisedmatcontM swipercont optgrey"></div>');
        jQuery('.personalisedmatcontM').append(buildGreySlide(personalisedmat, undefined, "personalisedmatM"));
        startSwiperMobile('.personalisedmatM', 2, 10);
        jQuery('.personalisedmatcontM').prepend('<h2>Personalised Mat</h2>');
    
    // Runners
    jQuery('.products-box').before('<div class="optgrey categorycont matrunnerscont"></div>');
    jQuery('.matrunnerscont').append(buildcategory(matrunners));
    jQuery('.matrunnerscont').prepend('<h2>Mat Runners</h2>');
         //mobile
        jQuery('.optMobileCont').append('<div class="matrunnerscontM swipercont optgrey"></div>');
        jQuery('.matrunnerscontM').append(buildcategory(matrunners));
        jQuery('.matrunnerscontM').prepend('<h2>Mat Runners</h2>');

    // Specialty
    jQuery('.products-box').before('<div class="optgrey categorycont specialtymatscont"></div>');
    jQuery('.specialtymatscont').append(buildcategory(specialtymats));
    jQuery('.specialtymatscont').prepend('<h2>Speciality Mats</h2>');   
        //mobile
        jQuery('.optMobileCont').append('<div class="specialtymatcontM swipercont optgrey"></div>');
        jQuery('.specialtymatcontM').append(buildcategory(specialtymats));
        jQuery('.specialtymatcontM').prepend('<h2>Specialty Mats</h2>');

        jQuery('body').addClass('OptimizelyTest');
        jQuery(".OptimizelyTest .banners-box .slide-holder").addClass('flexslider');
        jQuery(".OptimizelyTest .banners-box .slide-holder .slide-list").addClass('slides').removeClass('slide-list');
        
        jQuery(".OptimizelyTest .banners-box .slide-holder .slides").html('');
        jQuery(".OptimizelyTest .banners-box .home-banner-callout").html('');
        jQuery(".OptimizelyTest .banners-box .switcher").remove();
        
        jQuery(".OptimizelyTest .banners-box .slide-holder").append('<a class="prev" href="#"></a>');
        jQuery(".OptimizelyTest .banners-box .slide-holder").append('<a class="next" href="#"></a>');
        
        jQuery("<h2 class='page-title'>Search our variety of mats</h2>").insertAfter(jQuery(".OptimizelyTest .top-page-information .shadow-top"));
        
            
            
        
        /***** Banner *****/
        banner_data = {
            'slides': {
                '1' :{
                    'title'     : 'Create an outdoor experience indoors',
                    'text'      : 'Artificial grass gives any setting that unique welcoming look',
                    'image'     : 'https://i.imgur.com/sXvZwPc.jpg',
                    'link'      : 'https://i.imgur.com/G5ERIm5.png',
                    'link_href' : '#'
                },
                '2' :{
                    'title'     : 'Prevent Slips and Falls',
                    'text'      : 'Trap drirt and moisture at the door',
                    'image'     : 'https://i.imgur.com/2DHixlg.jpg',
                    'link'      : 'https://i.imgur.com/G5ERIm5.png',
                    'link_href' : '#'
                },
                '3' :{
                    'title'     : 'Increase Productivity and Safety',
                    'text'      : 'Anti Fatigue',
                    'image'     : 'https://i.imgur.com/3XRDQbj.jpg',
                    'link'      : 'https://i.imgur.com/G5ERIm5.png',
                    'link_href' : '#'
                }
            },
            'videos': {
                '1' :{
                    'image' : 'https://i.imgur.com/xeN9COH.jpg',
                    'link'  : 'https://www.youtube.com/embed/tT7ipT-5OsY'
                },
                '2' :{
                    'image' : 'https://i.imgur.com/mGqYOgS.jpg',
                    'link'  : 'https://www.youtube.com/embed/S4BozPnX_aI'
                },
                '3' :{
                    'image' : 'https://i.imgur.com/1Ui93Es.jpg',
                    'link'  : 'https://www.youtube.com/watch?v=Blpku1C9Db8'
                }
            }
        };
        
        Banner(banner_data);
        
        /**************************************
                    Banner
        **************************************/
        function Banner(data){
            
       
        
        
        
            /***** Slides Layout *****/
            var i=1; while(data['slides'][i]){
                jQuery(".OptimizelyTest .banners-box .switcher").append('<li><a href="#"></a></li>');
                jQuery(".OptimizelyTest .banners-box .slide-holder .slides").append('<li style="display:none;" class="slide-'+i+'"><img src="'+data['slides'][i]['image']+'" /></li>');
                if(data['slides'][i]['title'] != ''){
                    jQuery(".OptimizelyTest .banners-box .slide-holder .slides .slide-" + i + " ").append('<div class="caption" />');    
                };
                jQuery(".OptimizelyTest .banners-box .slide-holder .slides .slide-" + i + " .caption").append('<div class="inner"><h2>'+data['slides'][i]['title']+'</h2><p>'+data['slides'][i]['text']+'</p><a class="button" href="'+data['slides'][i]['link_href']+'"><img src="'+data['slides'][i]['link']+'" /></a></div>');
                i++;
            };
            
            jQuery(".OptimizelyTest .banners-box .slide-holder .slides li").first().css('display','block').addClass('active');
            
            
            
            
            /***** Videos Layout *****/
            var i=1; while(data['videos'][i]){
                jQuery(".OptimizelyTest .banners-box .home-banner-callout").append('<a data-fancybox href="'+data['videos'][i]['link']+'&autoplay=1&rel=0&controls=0&showinfo=0" class="fancybox"><img src="'+data['videos'][i]['image']+'" /><span class="play-icon"></span></a>');
                i++;
            };
            
            
            
            
            /***** Load External Scripts *****/
            function getScript(src, callback) {
                var s = document.createElement('script');
                s.src = src;
                s.async = true;
                s.onreadystatechange = s.onload = function() {
                    if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
                        callback.done = true;
                        callback();
                    }
                };
                document.querySelector('head').appendChild(s);
            };
            
            
            /** Flexslider **/
            jQuery('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css" />')
            getScript("https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider-min.js",start_flexslider);
            
            function start_flexslider(){
                jQuery('div.image-gallery').flexslider({
                    directionNav: false,
                    smoothHeight: true,
                    slideshow: true,
                    start: function(slider){	
                        jQuery('.OptimizelyTest .banners-box .slide-holder a.next').click(function(event){
                            event.preventDefault();
                            slider.flexAnimate(slider.getTarget("next"));
                        });
                        jQuery('.OptimizelyTest .banners-box .slide-holder a.prev').click(function(event){
                            event.preventDefault();
                            slider.flexAnimate(slider.getTarget("previous"));
                        });
                    }
                });
            };
            
            
            /** Fancybox **/
            jQuery('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css" />')
            getScript("https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js", null);
            jQuery(document).ready(function(){
                jQuery(".OptimizelyTest .banners-box .home-banner-callout a").css('height',jQuery(".OptimizelyTest .banners-box .slide-holder").height() / 3 - 4);
            });
        };
    
}

// Builds a slider from HTML
function htmlSlidesBuilder (arr, addClassToSwiper, slidename) {
    var html = ''
    
    for (var i = 0; i < arr.length; i++) {
        var slide = '<div class="swiper-slide"><ul>'+arr[i]+'</ul></div>'
        html += slide;
    }
    
    var swiperhtml = '<div class="swiper-container '+addClassToSwiper+' '+slidename+'"><div class="swiper-wrapper">'+html+'</div><div class="swiper-pagination"></div></div><div class="'+slidename+' swiper-button-prev"></div><div class="'+slidename+' swiper-button-next"></div></div>'
    return swiperhtml

}
// Gets the data from html into an array
function htmlSlidesArr (selector, callback) {
    var list = jQuery(selector),
        arr = [];
    for (var i = 0; i < list.length; i++) {
        var html = list[i].outerHTML;
        arr.push(html);
    }

    if (callback) {
        callback()
    } else {
        return arr;
    }
}

function buildmobile (arr) {
    var html = ''
    for (var i = 0; i < arr.length; i++) {
            var tile = '<div class="optTile"><div class="optTop"><div class="optTopImg" style="background-image:url('+arr[i].img+')"></div><p>'+arr[i].name+'</p></div><div class="optBottom"><a class="button" href="'+arr[i].url+'"><span><span>More Details</span></span></div></a></div>'   
            html += tile;
    }
    var finalhtml = '<div class="optMobileCont">'+html+'</div>'
    return finalhtml
}


function buildGreySlide (arr, addClassToSwiper, slidename) {
    var html = ''
    
    for (var i = 0; i < arr.length; i++) {
        var slide = '<div class="swiper-slide"><div class="optTile"><div class="optTop"><div class="optTopImg" style="background-image:url('+arr[i].img+')"></div><div class="optPback"><p>'+arr[i].name+'</p><p class="optArrow">></p></div></div><div class="optBottom"><a class="button" href="'+arr[i].url+'"><span><span>More Details</span></span></a></div></div></div>'
        html += slide;
    }
    
    var swiperhtml = '<div class="swiper-container '+addClassToSwiper+' '+slidename+'"><div class="swiper-wrapper">'+html+'</div><div class="swiper-pagination"></div></div><div class="'+slidename+' swiper-button-prev"></div><div class="'+slidename+' swiper-button-next"></div></div>'
    return swiperhtml


}

function buildproductslide (arr, addClassToSwiper, slidename) {
    var html = ''
    
    for (var i = 0; i < arr.length; i++) {
        var slide = '<div class="swiper-slide"><div class="optTile"><div class="optTop"><div class="optImage"><img src="'+arr[i].img+'"/></div><div class="optName">'+arr[i].name+'                        </div><div class="optPrice"><p class="optRed">'+arr[i].price+'</p><p class="optgst">incl. <b>GST</b></p></div></div><div class="optBottom"><a class="button" href="'+arr[i].url+'"><span><span>More Details</span></span></a></div></div></div>'        
        html += slide;

    }
    
    var swiperhtml = '<div class="swiper-container '+addClassToSwiper+' '+slidename+'"><div class="swiper-wrapper">'+html+'</div><div class="swiper-pagination"></div></div><div class="'+slidename+' swiper-button-prev"></div><div class="'+slidename+' swiper-button-next"></div></div>'   
     return swiperhtml

}

function buildcategory (arr) {
    rightcont = ''

    for (var i = 0; i < arr[0].items.length; i++) {
        listitem = '<li><a href='+arr[0].items[i].url+'>'+arr[0].items[i].name+'</a></li>'
        rightcont += listitem;
    }

    newrightcont = '<div class="catcont"><ul>'+rightcont+'</ul></div>'
    
    html = '<div class="optCategoryCont"><div class="optleft"><div class="optTop"><div class="optCatImage" style="background:url('+arr[1].leftcont[0].img+')"></div></div><div class="optBottom"><a class="button" href="'+arr[1].leftcont[0].url+'"><span><span>More Details</span></span></a></div></div><div class="optRight">'+newrightcont+'</div></div>'
        return html
}

// Starts a swiper
// Add the name of the swiper and how many slides it should show
function startSwiper(swipername, slides, space){
container = swipername+".swiper-container";
swipern = swipername.replace('.','');
console.log(swipern);
swipern = new Swiper(container, {
      speed: 400,
      spaceBetween: space,
      loop: true,
      slidesPerView: slides,
      pagination: ".swiper-container .swiper-pagination",
      paginationClickable: true,
      nextButton: ""+swipername+".swiper-button-next",
      prevButton: ""+swipername+".swiper-button-prev",
  });
}

function startSwiperMobile(swipername, slides, space, size){
container = swipername+".swiper-container";
swipern = swipername.replace('.','');
console.log(swipern);
swipern = new Swiper(container, {
      speed: 400,
      spaceBetween: space,
      loop: true,
      slidesPerView: slides,
      pagination: ".swiper-container .swiper-pagination",
      paginationClickable: true,
      nextButton: ""+swipername+".swiper-button-next",
      prevButton: ""+swipername+".swiper-button-prev",
      width: 550
  });
}

function startMobilebestsellers(swipername, slides, space, size){
    container = swipername+".swiper-container";
    swipern = swipername.replace('.','');
    console.log(swipern);
    swipern = new Swiper(container, {
          speed: 400,
          spaceBetween: space,
          loop: true,
          slidesPerView: slides,
          pagination: ".swiper-container .swiper-pagination",
          paginationClickable: true,
          nextButton: ""+swipername+".swiper-button-next",
          prevButton: ""+swipername+".swiper-button-prev",
          width: 400
      });
    }

function getScript(src, callback) {
  var s = document.createElement('script');
  s.src = src;
  s.async = true;
  s.onreadystatechange = s.onload = function() {
    if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
      callback.done = true;
      callback();
    }
  };
  document.querySelector('head').appendChild(s);
}


