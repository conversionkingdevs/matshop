
function returnDay (int) {
    switch (int) {
        case 0:
            return 'Sunday';
            break;
        case 1:
            return 'Monday';
            break;
        case 2:
            return 'Tuesday';
            break;
        case 3:
            return 'Wednesday';
            break;
        case 4:
            return 'Thursday';
            break;
        case 5:
            return 'Friday';
            break;
        case 6:
            return 'Saturday';
            break;
    }
}

jQuery.ajax({
    type: 'GET',
    url:document.location, // FOR LIVE CHANGE THIS TO document.location
    success: function(data, textStatus, request){
        
        // Time string returned from server
        var dateString = request.getResponseHeader('Date'),
            date = new Date(dateString),
            day = returnDay(date.getDay()),
            text = '';
            
        switch (day) {
            case "Monday":
            case "Tesuday":
            case "Wednesday":
                text = 'Have your mat by the end of week! Order today';
                break;
            case "Thursday":
            case "Friday":
            case "Saturday":
            case "Sunday":
                text = 'You\'ll have this item by Friday';
                break;
            default:
                text = 'We ship Daily';
                break;
        }

        var html = '<div class="opttext">'+text+'</div>';

        jQuery('.header-top').append(html);
        jQuery('.header .skype-phone.mobile').after(html);
        jQuery('.header .skype-phone.mobile').next().addClass('mobile');
    }
});

jQuery('.skype-phone.mobile a').text(' ');
