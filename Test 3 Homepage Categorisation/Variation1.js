/*    //HTML Structure
    // Container
    <div class="optCont optGreyBack">
        <h2>Find your mat</h2>
        <div class="optSwiperCont" id="findmat">
        </div>
    </div>

    // Slide
    <div class="slideCont">
        <div class="optTop">
            <p>Door Mats</p>
        </div>
        <div class="optBottom">
            <a href="//google.com" />
        </div>
    </div>
*/

var findYourMat = [
        {
            "url": "https://www.matshop.com.au/door-mats",
            "img": "https://www.matshop.com.au/media/catalog/category/Absorbent-Entrance-Mats..jpg",
            "name": "Door Mats"
        },{
            "url": "https://www.matshop.com.au/chair-mats",
            "img": "https://www.matshop.com.au/media/catalog/category/chair_mats_1.JPG",
            "name": "Chair Mats"
        },{
            "url": "https://www.matshop.com.au/chair-mats",
            "img": "https://www.matshop.com.au/media/catalog/category/Anti-Fatigue-Mat_2_1.jpg",
            "name": "Anti Fatigue Mats"
        },{
            "url": "https://www.matshop.com.au/non-slip-mats",
            "img": "https://www.matshop.com.au/media/catalog/category/Anti-Slip-Mats_1.jpg",
            "name": "Anti Slip Mats"
        },{
            "url": "https://www.matshop.com.au/personalised-door-mats",
            "img": "https://www.matshop.com.au/media/catalog/category/Custom-Printed-Mat_1.jpg",
            "name": "Personalised Mats"
        },{
            "url": "https://www.matshop.com.au/mat-runners",
            "img": "https://www.matshop.com.au/media/catalog/category/Mat-Runners_1.jpg",
            "name": "Mat Runners Mats"
        },{
            "url": "https://www.matshop.com.au/specialty-mats",
            "img": "https://www.matshop.com.au/media/catalog/category/Specialty-Mats_1.jpg",
            "name": "Specialised Mats"
        },{
            "url": "https://www.matshop.com.au/accessories",
            "img": "https://www.matshop.com.au/media/catalog/product/cache/1/small_image/140x/9df78eab33525d08d6e5fb8d27136e95/d/u/durable-non-slip-grit-surface-tape.png",
            "name": "Accessories"
        },
    ];

// Run the functions to build the test
function start() {
    jQuery('body').addClass('opt1');
     // Find your mat
    jQuery('.products-box').before('<div class="findyourmatcont swipercont optgrey"></div>')
    jQuery('.findyourmatcont').append(buildGreySlide(findYourMat, undefined, "findyourmat"));
    startSwiper('.findyourmat', 4, 10);
    jQuery('.findyourmatcont').prepend('<h2>Find Your Mat</h2>');
    jQuery('.nav-container').prepend(buildmobile(findYourMat))
    jQuery('.nav-container').prepend('<h2 class="mobileh2">Find your mat</h2>')
    // Build best sellers
    jQuery('.findyourmatcont').after('<div class="optbestsellerscont swipercont"></div>');
    jQuery('.optbestsellerscont').append(htmlSlidesBuilder(htmlSlidesArr('.products-box li'),"products-grid", "bestsellers"))
    startSwiper('.bestsellers', 5);
    jQuery('.bestsellers.swiper-container').prepend('<h2>Best Sellers</h2>');

   
}

// Builds a slider from HTML
function htmlSlidesBuilder (arr, addClassToSwiper, slidename) {
    var html = ''
    
    for (var i = 0; i < arr.length; i++) {
        var slide = '<div class="swiper-slide"><ul>'+arr[i]+'</ul></div>'
        html += slide;
    }
    
    var swiperhtml = '        <div class="swiper-container '+addClassToSwiper+' '+slidename+'"><div class="swiper-wrapper">'+html+'</div>            <div class="swiper-pagination"></div>        </div>            <div class="'+slidename+' swiper-button-prev"></div>            <div class="'+slidename+' swiper-button-next"></div>        </div>      '
    return swiperhtml

}
// Gets the data from html into an array
function htmlSlidesArr (selector, callback) {
    var list = jQuery(selector),
        arr = [];
    for (var i = 0; i < list.length; i++) {
        var html = list[i].outerHTML;
        arr.push(html);
    }

    if (callback) {
        callback()
    } else {
        return arr;
    }
}

function buildmobile (arr) {
    var html = ''
    for (var i = 0; i < arr.length; i++) {
            var tile = '<div class="optTile"><div class="optTop"><div class="optTopImg" style="background-image:url('+arr[i].img+')"></div><p>'+arr[i].name+'</p></div><div class="optBottom"><a class="button" href="'+arr[i].url+'"><span><span>More Details</span></span></div></a></div>'   
            html += tile;
    }
    var finalhtml = '        <div class="optMobileCont">            '+html+'        </div>        '
    return finalhtml
}

function buildGreySlide (arr, addClassToSwiper, slidename) {
    var html = ''
    
    for (var i = 0; i < arr.length; i++) {
        var slide = '<div class="swiper-slide"><div class="optTile"><div class="optTop"><div class="optTopImg" style="background-image:url('+arr[i].img+')"></div><p>'+arr[i].name+'</p></div><div class="optBottom"><a class="button" href="'+arr[i].url+'"><span><span>More Details</span></span></div></a></div></div>'
        html += slide;
    }
    
    var swiperhtml = '        <div class="swiper-container '+addClassToSwiper+' '+slidename+'">            <div class="swiper-wrapper">                '+html+'            </div>            <div lass="swiper-pagination"></div>            </div>            <div class="'+slidename+' swiper-button-prev"></div>            <div class="'+slidename+' swiper-button-next"></div>        </div>  '
    return swiperhtml


}

// Starts a swiper
// Add the name of the swiper and how many slides it should show
function startSwiper(swipername, slides, space){
container = swipername+".swiper-container";
console.log(container)
mainSwiper= new Swiper(container, {
      speed: 400,
      spaceBetween: space,
      loop: true,
      slidesPerView: slides,
      pagination: ".swiper-container .swiper-pagination",
      paginationClickable: true,
      nextButton: ""+swipername+".swiper-button-next",
      prevButton: ""+swipername+".swiper-button-prev"
  });
}

function getScript(src, callback) {
  var s = document.createElement('script');
  s.src = src;
  s.async = true;
  s.onreadystatechange = s.onload = function() {
    if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
      callback.done = true;
      callback();
    }
  };
  document.querySelector('head').appendChild(s);
}

jQuery("head").append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/css/swiper.min.css">');
getScript("https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/js/swiper.min.js",start);

