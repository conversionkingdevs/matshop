
var useCaseToCategory = {
    "Carpet & Underlay 13 - 30mm thick":    "Carpet",
    "Carpet & Underlay 7 - 12mm thick":     "Carpet",
    "Carpet & Underlay up to 6mm thick":    "Carpet",
    "Under Chairs on Carpet Floors":        "Carpet",
    "Ute Trays":    "Vehicles",
    "Van backs":    "Vehicles",
    "Arcades":      "Commercial",
    "Banks":        "Commercial",
    "Bars":         "Commercial",
    "Bathrooms":    "Commercial",
    "Commercial Buildings":     "Commercial",
    "Service Stations":         "Commercial",
    "Small Shops":              "Commercial",
    "Takeaway Shops":           "Commercial",
    "Food Preparation Areas":       "Commercial",
    "Commercial Kitchens":          "Commercial",
    "Beverage Dispensary areas":    "Commercial",
    "Long Term Promotions":         "Custom Print",
    "Temporary Promotions":         "Custom Print",
    "Under Chairs on Hard Floors":      "Hard Floor",
    "Heavy Traffic Doorways":           "Heavy Traffic",
    "Very High Traffic Doorways":       "Heavy Traffic",
    "Restaurants":                  "High Traffic",
    "Assembly Areas":               "High traffic",
    "Corridors":                "Indoor",
    "Hallways":                 "Indoor",
    "Factory Doorways":         "Industrial",
    "Industrial workshops":     "Industrial",
    "Warehouse Doorways":       "Industrial",
    "Factories":            "Industrial",
    "Production Lines":     "Industrial",
    "Warehouses":           "Industrial",
    "Foundarys":            "Industrial",
    "Light Traffic Doorways":       "Light Traffic",
    "Medium Traffic Doorways":      "Medium Traffic",
    "Outdoor Areas":                "Outdoor",
    "Ramps":                    "Outdoor",
    "Electrical Risk Areas":    "Safety",
    "Static Control Areas":     "Safety",
    "Covering Cables":      "Safety",
    "Gym":                  "Service",
    "Fitness Studio":       "Service",
    "Colleges":     "Services",
    "Hotels":       "Services",
    "Motels":       "Services",
    "Resorts":      "Services",
    "Schools":      "Services",
    "Shopping Centres":    "Services"
}

var imageTo = [
    {"filename":"arcade2.png","url":"//cdn.optimizely.com/img/8505590826/10367aa9c669475baeef379508510f02.png"},
    
    {"filename":"arcade3.png","url":"//cdn.optimizely.com/img/8505590826/baf080e63f594e968b6437882251e5e9.png"},
    
    {"filename":"bank3.png","url":"//cdn.optimizely.com/img/8505590826/098a9479b44d4fc4b724e47a1d1b4083.png"},
    
    {"filename":"carpet_white.png","url":"//cdn.optimizely.com/img/8505590826/e6a8abb949f64a758bd8b7c5cb334ba1.png"},
    
    {"filename":"carpet_orange.png","url":"//cdn.optimizely.com/img/8505590826/33876863a0d143dfbf415f45d29fbedd.png"},
    
    {"filename":"carpet_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/446ac6ae5a3241368cd092e34bd7b625.png"},
    
    {"filename":"Carpet_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/6e1145af3dad45528910e3b1d771a729.png"},
    
    {"filename":"commercial_orange.png","url":"//cdn.optimizely.com/img/8505590826/ba7775010e70489896081b9da70328e1.png"},
    
    {"filename":"commercial_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/4ae62f22072647b09ff76d2acbb57612.png"},
    
    {"filename":"commercial_white.png","url":"//cdn.optimizely.com/img/8505590826/a2ebfd9c5c3b4ca0b81768c4d9f15b79.png"},
    
    {"filename":"commercial_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/73ccc6dfe2414ea2b860f4cbf149c909.png"},
    
    {"filename":"Customprint_orange.png","url":"//cdn.optimizely.com/img/8505590826/6ebd1c9cd44c4911badfb756a5b98f5a.png"},
    
    {"filename":"customprint_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/03c00de355cf41cc8d7ec4b17e3d4066.png"},
    
    {"filename":"Customprint_white.png","url":"//cdn.optimizely.com/img/8505590826/c8a3e017fff746f1990b5d7d7ed83178.png"},
    
    {"filename":"Customprint_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/2c42d73421b44d27896c5e2211d4d341.png"},
    
    {"filename":"CustomSize_orange.png","url":"//cdn.optimizely.com/img/8505590826/1191ab4d56f44b37a4bcb185853b764e.png"},
    
    {"filename":"Customsize_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/32c1e626fe16485d8f65e27276dd5c3a.png"},
    
    {"filename":"CustomSize_white.png","url":"//cdn.optimizely.com/img/8505590826/ef1846e4abb641fdb5aef3a1e91f6926.png"},
    
    {"filename":"HardFloor_orange.png","url":"//cdn.optimizely.com/img/8505590826/227de23115d54b3198810698800d895c.png"},
    
    {"filename":"hardfloor_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/33365e81e64c4780957f744d2b38c1da.png"},
    
    {"filename":"HardFloor_white.png","url":"//cdn.optimizely.com/img/8505590826/7e3ab548a5314e3a83eac3c951a832e3.png"},
    
    {"filename":"hardfloor_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/697c196c35884fb28f2ece06f816b34c.png"},
    
    {"filename":"Heavytraffic_orange.png","url":"//cdn.optimizely.com/img/8505590826/667452ba618443ed81d4f2c7d5a23ebc.png"},
    
    {"filename":"heavytraffic_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/f27ef138b87543ec8b9fa0e58f597b98.png"},
    
    {"filename":"heavytraffic_white.png","url":"//cdn.optimizely.com/img/8505590826/70ae895eddba461c89b81f3fc0bfeb19.png"},
    
    {"filename":"heavytraffic_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/ffb612cde204468796d9931a06121532.png"},
    
    {"filename":"HighTraffic_orange.png","url":"//cdn.optimizely.com/img/8505590826/f27bd307a2494967bbe316baa30322e2.png"},
    
    {"filename":"hightraffic_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/0e9a4f67953147d895b18435467e4f9a.png"},
    
    {"filename":"HighTraffic_white.png","url":"//cdn.optimizely.com/img/8505590826/a2f4a1327c8c4ce0b3e5920f5bb02b6b.png"},
    
    {"filename":"Hightraffic_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/51b4c1294ea141aea7de111c884aaf0a.png"},
    
    {"filename":"indoor_orange.png","url":"//cdn.optimizely.com/img/8505590826/3f8ff063eda948f4840a835934bfcf93.png"},
    
    {"filename":"Indoor_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/a392ee8f3a5c490fae292f7fd2358f66.png"},
    
    {"filename":"indoor_white.png","url":"//cdn.optimizely.com/img/8505590826/ed81dcc9f5344d61a63b2509d6758427.png"},
    
    {"filename":"indoor_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/5519186655574b41a2f294cc86c802d9.png"},
    
    {"filename":"indoorwhite.png","url":"//cdn.optimizely.com/img/8505590826/e281c0e70b2947d8ac67a5c0375f8fed.png"},
    
    {"filename":"industrial_orange.png","url":"//cdn.optimizely.com/img/8505590826/f58abcce49a246d7b60943412837fa3d.png"},
    
    {"filename":"industrial_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/fc8b678684454d92acef350e05026727.png"},
    
    {"filename":"industrial_white.png","url":"//cdn.optimizely.com/img/8505590826/4518c1a34d824c5e897378284f2efd92.png"},
    
    {"filename":"Industrial_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/7225733473ae4d1f9bfe9559195f6faf.png"},
    
    {"filename":"industrial.png","url":"//cdn.optimizely.com/img/8505590826/b4b72d1ee33a42b085c172ec9498435b.png"},
    
    {"filename":"Lightraffic_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/9a63939f7dab4908836c25c03ca67330.png"},
    
    {"filename":"lighttraffic_orange.png","url":"//cdn.optimizely.com/img/8505590826/91badb029d0d4a68a8ea2093fb944da1.png"},
    
    {"filename":"lighttraffic_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/b36c5d8d2fdc4256bff84794afc26700.png"},
    
    {"filename":"lighttrafficwhite.png","url":"//cdn.optimizely.com/img/8505590826/f3cec5297c8a44899a01dc2f0e403a98.png"},
    
    {"filename":"MediumTraffic_orange.png","url":"//cdn.optimizely.com/img/8505590826/837efa6243d841ef85d7f5838a3311bc.png"},
    
    {"filename":"mediumtraffic_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/e6e09a0b7d9d4a3792c4e5df59afdbd4.png"},
    
    {"filename":"MediumTraffic_white.png","url":"//cdn.optimizely.com/img/8505590826/a0d2b73222cd454fb15cca1a815207db.png"},
    
    {"filename":"Mediumtraffic_whiteSOLO.PNG","url":"//cdn.optimizely.com/img/8505590826/4bc5142c2b8242a4acdfae785146fd44.png"},
    
    {"filename":"MediumTraffic.png","url":"//cdn.optimizely.com/img/8505590826/4cb3713846cd4f5d840a3e879e16dab9.png"},
    
    {"filename":"outdoor_orange.png","url":"//cdn.optimizely.com/img/8505590826/6fa7d1ac1906420bafbca8bf83f3829d.png"},
    
    {"filename":"outdoor_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/0e0c60e5e0c94c9ea80a1b88876727ee.png"},
    
    {"filename":"outdoor_white.png","url":"//cdn.optimizely.com/img/8505590826/8eb714f5d65c497eb8b326f6ee9f1282.png"},
    
    {"filename":"outdoor_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/b06849cd84d8414a98a78b3d0b21e08b.png"},
    
    {"filename":"outdoor.png","url":"//cdn.optimizely.com/img/8505590826/a840a1db0619400892573333ba1942ba.png"},
    
    {"filename":"pub3.png","url":"//cdn.optimizely.com/img/8505590826/200886e565f64be9ab6c7fc9349c84e9.png"},
    
    {"filename":"safety_orange.png","url":"//cdn.optimizely.com/img/8505590826/bfecaef1a9cf40828d53115ed21bba1e.png"},
    
    {"filename":"safety_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/fd0573cb5cca449ca382b9ea86ba52b3.png"},
    
    {"filename":"Safety_white.png","url":"//cdn.optimizely.com/img/8505590826/d524658e32b349a196db3c094341080e.png"},
    
    {"filename":"safety_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/711bd67cd469407d9df68b2be03e9f36.png"},
    
    {"filename":"service_orange.png","url":"//cdn.optimizely.com/img/8505590826/6a6cf7cb906e49f299d2deb91cc9b615.png"},
    
    {"filename":"service_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/025828fa238b4ae4a65a4014a2d1878a.png"},
    
    {"filename":"service_white.png","url":"//cdn.optimizely.com/img/8505590826/3805aedef9ac4f179566a249eb38fcb5.png"},
    
    {"filename":"service_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/f237dad2a08e4246a95fc3e37ddfc241.png"},
    
    {"filename":"Vehicle_orange.png","url":"//cdn.optimizely.com/img/8505590826/16be5b8dbd0d4265ae54578d69943ff0.png"},
    
    {"filename":"vehicle_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/f173507bc76f4a80ba9859fae2ccd42a.png"},
    
    {"filename":"vehicle_white.png","url":"//cdn.optimizely.com/img/8505590826/0f2af951b35c4711915d72ce7ccb9a9b.png"},
    
    {"filename":"vehicle_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/77cc84413b6441a6be14956b850884e2.png"}
]

var categoryToImage = {
    "Carpet":  [
        {"filename":"carpet_white.png","url":"//cdn.optimizely.com/img/8505590826/e6a8abb949f64a758bd8b7c5cb334ba1.png"},
        {"filename":"carpet_orange.png","url":"//cdn.optimizely.com/img/8505590826/33876863a0d143dfbf415f45d29fbedd.png"},
        {"filename":"carpet_orangeSOLO.png","url":"//cdn.optimizely.com/img/8505590826/446ac6ae5a3241368cd092e34bd7b625.png"},
        {"filename":"Carpet_whiteSOLO.png","url":"//cdn.optimizely.com/img/8505590826/6e1145af3dad45528910e3b1d771a729.png"},
    ],
    "Commercial":       [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Custom Print":     [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Custom Size":      [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Hard Floor":       [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Heavy Traffic":    [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "High traffic":     [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Indoor":           [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Industrial":       [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Light Traffic":    [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Medium Traffic":   [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Outdoor":          [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Safety":           [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Services":         [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ],
    "Vehicles":         [
        "http://via.placeholder.com/30x30?text=v1",
        "http://via.placeholder.com/30x30?text=v2",
        "http://via.placeholder.com/30x30?text=v3",
        "http://via.placeholder.com/30x30?text=v4"
    ]
}

var productList = [  
    {  
       "link":"https://www.matshop.com.au/ribbed-carpet-entrance-mat-runner-900mm-wide-custom-length",
       "data":[  
          "Arcades",
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Corridors",
          "Factory Doorways",
          "Hallways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Restaurants",
          "Service Stations",
          "Shopping Centres",
          "Warehouse Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-berber-needle-punch-commercial-entrance-mat-with-pvc-backing",
       "data":[  
          "Arcades",
          "Banks",
          "Colleges",
          "Hotels",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/ribbed-carpet-entrance-mat-runner-1800mm-wide-custom-length",
       "data":[  
          "Arcades",
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Corridors",
          "Factory Doorways",
          "Hallways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Restaurants",
          "Service Stations",
          "Shopping Centres",
          "Warehouse Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/ribbed-carpet-entrance-mat-runner-1200mm-wide-custom-length",
       "data":[  
          "Arcades",
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Corridors",
          "Factory Doorways",
          "Hallways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Restaurants",
          "Service Stations",
          "Shopping Centres",
          "Warehouse Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/high-absorbent-reinforced-rubber-backed-mat",
       "data":[  
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Heavy Traffic Doorways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Small Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/commercial-clean-dry-scrape-high-traffic-entrance-mat",
       "data":[  
          "Banks",
          "Colleges",
          "Commercial Buildings",
          "Heavy Traffic Doorways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/durable-nylon-twist-pile-welcome-mat-with-rubber-backing",
       "data":[  
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Hotels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Small Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/the-aluminium-architectural-entrance-mat-for-high-traffic",
       "data":[  
          "Banks",
          "Colleges",
          "Commercial Buildings",
          "Heavy Traffic Doorways",
          "Hotels",
          "Resorts",
          "Restaurants"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/commercial-clean-dry-scrape-entrance-mat",
       "data":[  
          "Banks",
          "Colleges",
          "Hotels",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres",
          "Very High Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/high-absorbent-reinforced-rubber-backed-mat-252",
       "data":[  
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Heavy Traffic Doorways",
          "Hotels",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/pvc-loop-pile-mats",
       "data":[  
          "Bathrooms",
          "Colleges",
          "Commercial Buildings",
          "Factory Doorways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Outdoor Areas"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/sticky-lab-surgical-dirt-adhesive-mat",
       "data":[  
          "Food Preparation Areas",
          "Industrial workshops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/nylon-twist-pile-rubber-backed-mat",
       "data":[  
          "Heavy Traffic Doorways",
          "Light Traffic Doorways",
          "Very High Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/plush-pile-polypropylene-mat",
       "data":[  
          "Light Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/ribbed-pile-polypropylene-mat",
       "data":[  
          "Light Traffic Doorways",
          "Service Stations",
          "Small Shops",
          "Very High Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/herringbone-pattern-polypropylene-mat",
       "data":[  
          "Light Traffic Doorways",
          "Medium Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/low-profile-outdoor-scraper-mat",
       "data":[  
          "Bars",
          "Commercial Buildings",
          "Commercial Kitchens",
          "Heavy Traffic Doorways",
          "Outdoor Areas",
          "Ramps",
          "Schools",
          "Service Stations",
          "Warehouse Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/rubber-outdoor-scraper-mat",
       "data":[  
          "Commercial Buildings",
          "Factory Doorways",
          "Heavy Traffic Doorways",
          "Hotels",
          "Outdoor Areas",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Takeaway Shops",
          "Warehouse Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/custom-coir-mat-17mm-thick",
       "data":[  
          "Arcades",
          "Colleges",
          "Commercial Buildings",
          "Factory Doorways",
          "Heavy Traffic Doorways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Restaurants",
          "Schools"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/custom-coir-mat",
       "data":[  
          "Arcades",
          "Colleges",
          "Commercial Buildings",
          "Gym",
          "Hallways",
          "Schools"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/custom-coir-mat-30mm-thick",
       "data":[  
          "Arcades",
          "Colleges",
          "Motels",
          "Restaurants",
          "Schools",
          "Small Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/custom-size-coir-mat-17mm-thick-black",
       "data":[  
          "Arcades",
          "Assembly Areas",
          "Banks",
          "Colleges",
          "Factory Doorways",
          "Heavy Traffic Doorways",
          "Schools",
          "Warehouse Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/printed-coir-logo-mat",
       "data":[  
          "Banks",
          "Hotels",
          "Light Traffic Doorways",
          "Medium Traffic Doorways",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/coir-scraper-mat-with-woven-sides-600-x-1000-x-40mm-thick",
       "data":[  
          "Colleges",
          "Heavy Traffic Doorways",
          "Hotels",
          "Motels",
          "Outdoor Areas",
          "Restaurants",
          "Schools",
          "Very High Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/pvc-loop-pile-mat-runner-with-non-slip-rubberised-backing",
       "data":[  
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Takeaway Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/coir-scraper-mat-with-reinforced-edges",
       "data":[  
          "Schools",
          "Takeaway Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-ribbed-polypropylene-mat-for-high-traffic-entrances-317",
       "data":[  
          "Heavy Traffic Doorways",
          "Medium Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/hard-wearing-poly-propylene-synthetic-coir-entrance-mat",
       "data":[  
          "Banks",
          "Colleges",
          "Factory Doorways",
          "Hotels",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Small Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/aluminium-mat-well-frame-metal-trim",
       "data":[  
          "Arcades",
          "Banks",
          "Schools",
          "Shopping Centres",
          "Very High Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/high-traffic-dual-zone-dry-brush-designer-entrance-mat",
       "data":[  
          "Banks",
          "Colleges",
          "Commercial Buildings",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Schools"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-pvc-loop-mat-drainable",
       "data":[  
          "Fitness Studio",
          "Gym",
          "Hotels",
          "Outdoor Areas",
          "Resorts"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/nylon-carpet-surface-custom-printed-logo-mat",
       "data":[  
          "Banks",
          "Beverage Dispensary areas",
          "Colleges",
          "Commercial Buildings",
          "Factory Doorways",
          "Hotels",
          "Long Term Promotions",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/polypropylene-crisp-imprint-custom-logo-mat",
       "data":[  
          "Banks",
          "Colleges",
          "Commercial Buildings",
          "Factory Doorways",
          "Long Term Promotions",
          "Medium Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-reinforced-logo-mat",
       "data":[  
          "Banks",
          "Colleges",
          "Commercial Buildings",
          "Heavy Traffic Doorways",
          "Long Term Promotions",
          "Schools",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/nylon-carpet-surface-custom-printed-logo-mat-267",
       "data":[  
          "Banks",
          "Beverage Dispensary areas",
          "Colleges",
          "Commercial Buildings",
          "Factory Doorways",
          "Hotels",
          "Long Term Promotions",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/custom-printed-low-profile-outdoor-rubber-scraper-mat",
       "data":[  
          "Colleges",
          "Commercial Buildings",
          "Factory Doorways",
          "Heavy Traffic Doorways",
          "Resorts",
          "Schools",
          "Service Stations"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/high-traffic-inlaid-berber-logo-mat",
       "data":[  
          "Hotels",
          "Long Term Promotions",
          "Motels",
          "Resorts",
          "Restaurants",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-thick-pile-carpet-910-x-1200mm-keyhole-shape",
       "data":[  
          "Carpet & Underlay 13 - 30mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-thick-pile-carpet-1170-x-1500mm-rectangle-shape",
       "data":[  
          "Carpet & Underlay 13 - 30mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-thick-pile-carpet-1130-x-1340mm-keyhole-shape",
       "data":[  
          "Carpet & Underlay 13 - 30mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-medium-pile-carpet-910-x-1200mm-keyhole-shape",
       "data":[  
          "Carpet & Underlay 7 - 12mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-medium-pile-carpet-1130-x-1340mm-keyhole-shape",
       "data":[  
          "Carpet & Underlay 7 - 12mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-medium-pile-carpet-1170-x-1500mm-rectangle-shape",
       "data":[  
          "Carpet & Underlay 7 - 12mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-clear-budget-pvc-for-carpet-920-x-1220mm",
       "data":[  
          "Carpet & Underlay 7 - 12mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-clear-value-pvc-for-carpet-1150-x-1350mm",
       "data":[  
          "Carpet & Underlay 7 - 12mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-low-pile-carpets-910-x-1200mm-keyhole-shape",
       "data":[  
          "Carpet & Underlay up to 6mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-low-pile-carpets-1130-x-1340mm",
       "data":[  
          "Carpet & Underlay up to 6mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-low-pile-carpets-1170-x-1500mm-rectangle-shape",
       "data":[  
          "Carpet & Underlay up to 6mm thick",
          "Under Chairs on Carpet Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-chair-mat-for-low-pile-or-hard-floors-with-anti-fatigue-hinged-mat",
       "data":[  
          "Carpet & Underlay up to 6mm thick"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-hard-floors-1150-x-1350mm-keyhole-shape",
       "data":[  
          "Under Chairs on Hard Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-hard-floors-910-x-1200mm-keyhole-shape",
       "data":[  
          "Under Chairs on Hard Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-hard-floors-1160-x-1510mm-rectangle-shape",
       "data":[  
          "Under Chairs on Hard Floors"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-custom-size-for-medium-pile-carpets",
       "data":[  
          "Beverage Dispensary areas",
          "Carpet & Underlay 7 - 12mm thick",
          "Hallways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-custom-size-for-medium-pile-carpets-1500mm-wide",
       "data":[  
          "Beverage Dispensary areas",
          "Carpet & Underlay 7 - 12mm thick",
          "Hallways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-custom-size-for-hard-floors",
       "data":[  
          "Beverage Dispensary areas",
          "Hallways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/pvc-chair-mat",
       "data":[  
          "Carpet & Underlay 7 - 12mm thick",
          "Carpet & Underlay up to 6mm thick"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/custom-size-nylon-carpet-surface-custom-printed-logo-mat",
       "data":[  
          "Commercial Buildings",
          "Long Term Promotions",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres",
          "Small Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/high-comfort-custom-printed-logo-anti-fatigue-mat",
       "data":[  
          "Arcades",
          "Assembly Areas",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Long Term Promotions",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres",
          "Temporary Promotions"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/pvc-safety-mat-runner",
       "data":[  
          "Bars",
          "Factories",
          "Outdoor Areas"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/sheet-rubber-fine-rib-3mm-thick",
       "data":[  
          "Outdoor Areas",
          "Ute Trays",
          "Van backs"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/durable-rubber-ute-mat-liner-with-holes-1830mm-wide",
       "data":[  
          "Ute Trays",
          "Van backs"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/non-slip-anti-fatigue-safety-mat-runner",
       "data":[  
          "Bars",
          "Commercial Kitchens",
          "Factories",
          "Food Preparation Areas",
          "Hotels",
          "Restaurants",
          "Takeaway Shops",
          "Ute Trays",
          "Van backs"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/thick-pile-artificial-grass",
       "data":[  
          "Beverage Dispensary areas",
          "Colleges",
          "Commercial Buildings",
          "Fitness Studio",
          "Hotels",
          "Resorts",
          "Restaurants"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/thick-pile-artificial-grass-mat",
       "data":[  
          "Beverage Dispensary areas",
          "Colleges",
          "Commercial Buildings",
          "Fitness Studio",
          "Hotels",
          "Resorts",
          "Restaurants"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/sheet-rubber-wide-rib-6mm-thick",
       "data":[  
          "Outdoor Areas",
          "Ute Trays",
          "Van backs"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/plastic-ground-protection-track-mats-900-x-2400mm",
       "data":[  
          "Long Term Promotions",
          "Outdoor Areas",
          "Temporary Promotions"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/high-absorption-bar-top-runner",
       "data":[  
          "Bars",
          "Beverage Dispensary areas",
          "Commercial Kitchens",
          "Food Preparation Areas",
          "Hotels",
          "Motels",
          "Resorts",
          "Restaurants",
          "Service Stations",
          "Takeaway Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/low-pile-hardwearing-polypropylene-artificial-grass",
       "data":[  
          "Colleges",
          "Hotels",
          "Motels",
          "Outdoor Areas",
          "Resorts",
          "Schools",
          "Temporary Promotions"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/multi-purpose-anti-fatigue-rubber-mat-roll-with-drainage-holes",
       "data":[  
          "Beverage Dispensary areas",
          "Commercial Kitchens",
          "Factories",
          "Food Preparation Areas",
          "Production Lines",
          "Ute Trays",
          "Van backs"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/rubber-foot-bath-mat-with-high-yellow-sides",
       "data":[  
          "Commercial Kitchens",
          "Food Preparation Areas",
          "Restaurants"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/pvc-tubular-matting",
       "data":[  
          "Ramps"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/interlocking-rubber-tile-with-solid-top",
       "data":[  
          "Factories",
          "Gym",
          "Industrial workshops",
          "Resorts",
          "Schools",
          "Warehouses"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/the-non-conductive-electrical-safety-switchboard-matting",
       "data":[  
          "Electrical Risk Areas"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/anti-static-dissipative-comfort-mat",
       "data":[  
          "Static Control Areas"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/interlocking-spark-resistant-welding-mat",
       "data":[  
          "Factories",
          "Foundarys"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/ultra-comfort-nitrile-rubber-foam-non-slip-safety-mat",
       "data":[  
          "Bars",
          "Commercial Kitchens",
          "Factories",
          "Food Preparation Areas",
          "Hotels",
          "Production Lines",
          "Restaurants",
          "Takeaway Shops"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/rubber-wedge-threshold-ramps",
       "data":[  
          "Factories",
          "Factory Doorways",
          "Warehouse Doorways",
          "Warehouses"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/durable-anti-slip-grit-tape",
       "data":[  
          "Bars",
          "Bathrooms",
          "Beverage Dispensary areas",
          "Commercial Buildings",
          "Factories",
          "Ramps"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-plastic-cable-protection-cover",
       "data":[  
          "Assembly Areas",
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Covering Cables",
          "Factories",
          "Hotels",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-cable-cover",
       "data":[  
          "Covering Cables",
          "Factory Doorways",
          "Medium Traffic Doorways",
          "Schools"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/high-absorbent-reinforced-rubber-backed-mat-253",
       "data":[  
          "Banks",
          "Bars",
          "Colleges",
          "Commercial Buildings",
          "Heavy Traffic Doorways",
          "Hotels",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/ultimate-building-code-compliant-absorbent-entrance-mat",
       "data":[  
          "Banks",
          "Colleges",
          "Commercial Buildings",
          "Corridors",
          "Hallways",
          "Heavy Traffic Doorways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/ultimate-building-code-compliant-absorbent-entrance-mat-249",
       "data":[  
          "Banks",
          "Colleges",
          "Commercial Buildings",
          "Corridors",
          "Hallways",
          "Heavy Traffic Doorways",
          "Hotels",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/heavy-duty-ribbed-polypropylene-mat-for-high-traffic-entrances-318",
       "data":[  
          "Heavy Traffic Doorways",
          "Medium Traffic Doorways"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/nylon-carpet-surface-custom-printed-logo-mat-268",
       "data":[  
          "Banks",
          "Beverage Dispensary areas",
          "Colleges",
          "Commercial Buildings",
          "Factory Doorways",
          "Hotels",
          "Long Term Promotions",
          "Medium Traffic Doorways",
          "Motels",
          "Resorts",
          "Restaurants",
          "Schools",
          "Service Stations",
          "Shopping Centres"
       ]
    },
    {  
       "link":"https://www.matshop.com.au/chair-mat-heavy-duty-for-thick-pile-carpet-underlay-910-x-1200mm-keyhole-shape",
       "data":[  
          "Under Chairs on Carpet Floors"
       ]
    }
 ]

function getDataFromLink(link){
   for(var i = 0; i < productList.length; i++){
       if(productList[i]["link"] == link){
            return productList[i]["data"];
       }
   }
   return [];
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function buildCategoryList(list){ 
    var categoryList = [];
    for(var i = 0; i < list.length; i++){
        if(categoryList.indexOf(useCaseToCategory[list[i]]) == -1){
            categoryList.push(useCaseToCategory[list[i]]);
        }
    }
    return categoryList;
}

defer(function(){
    var variation = 0;
    jQuery("body").addClass("opt-"+variation);
    jQuery(".standalone-bottomline").parents(".item").addClass("opt-review");
    jQuery("#products-list .item").each(function(){
        var link = jQuery(this).find("a.product-image").attr("href");
        var list = getDataFromLink(link);
        var category = buildCategoryList(list);
        var categoryList = [];
        for(var i = 0; i < category.length && i < 3; i++){
            categoryList.push(category[i]);
            //console.log(categoryToImage[category[i]][0]);
        }
        console.log(categoryList);
    });

}, "#products-list");